UBOOT_SRC ?= "git://git@git.congatec.com/arm-nxp/imx8-family/uboot-imx8-family.git;protocol=https"
SRCBRANCH = "cgtimx8mm__imx_v2020.04_5.4.47_2.2.0"
SRC_URI = "${UBOOT_SRC};branch=${SRCBRANCH}"
SRCREV = "53923d58096634d5cd8fa696c3196e1b6299b3b9"
