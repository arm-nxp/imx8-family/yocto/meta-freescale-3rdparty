# Copyright (C) 2013-2016 Freescale Semiconductor
# Copyright 2017-2020 NXP
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "Linux Kernel provided and supported by NXP"
DESCRIPTION = "Linux Kernel provided and supported by NXP with focus on \
i.MX Family Reference Boards. It includes support for many IPs such as GPU, VPU and IPU."

require recipes-kernel/linux/linux-imx.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

DEPENDS += "lzop-native bc-native"

SRCBRANCH = "cgtimx8mm__imx_5.4.47_2.2.0"
LOCALVERSION = "-2.2.0"
KERNEL_SRC = "git://git@git.congatec.com/arm-nxp/imx8-family/kernel-imx8-family.git;protocol=https"
SRC_URI = "${KERNEL_SRC};branch=${SRCBRANCH}"

SRCREV = "1ade445ad9b5dbdb76c76b0a6a31aebbf1c285e3"

FILES_${KERNEL_PACKAGE_NAME}-base += "${nonarch_base_libdir}/modules/${KERNEL_VERSION}/modules.builtin.modinfo "

KERNEL_CONFIG_COMMAND = "oe_runmake_call -C ${S} CC="${KERNEL_CC}" O=${B} olddefconfig"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

DEFAULT_PREFERENCE = "1"

addtask copy_defconfig after do_patch before do_preconfigure

do_copy_defconfig () {
    install -d ${B}
    # copy sx8m defconfig
        mkdir -p ${B}
    cp ${S}/arch/arm64/configs/sx8m_defconfig ${B}/.config
    cp ${S}/arch/arm64/configs/sx8m_defconfig ${B}/../defconfig
}

COMPATIBLE_MACHINE = "(sx8m)"
